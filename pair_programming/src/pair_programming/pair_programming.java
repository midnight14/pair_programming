package pair_programming;

public class pair_programming {

	public static void main(String[] args) {
		int factorial = fact(3);
	      System.out.println("Factorial of 3 = "+factorial);
	   }
	   static int fact(int nums) {
		   //base case
	       if(nums==1){
	         return 1;
	       }
	       //Function calling itself
	       int output = fact(nums-1)* nums;
	       return output;

	}

}
